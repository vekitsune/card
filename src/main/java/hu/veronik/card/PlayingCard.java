/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.veronik.card;

/**
 *
 * @author User
 */
public class PlayingCard {
    private Suit suit;
    private Value value;

    @Override
    public String toString() {
        return "PlayingCard{" + "suit=" + suit + ", value=" + value + '}';
    }

    public Suit getSuit() {
        return suit;
    }

    public void setSuit(Suit suit) {
        this.suit = suit;
    }

    public Value getValue() {
        return value;
    }

    public void setValue(Value value) {
        this.value = value;
    }
    
    

    public boolean equals(PlayingCard other){
        boolean re=false;
        if((this.getValue()==other.getValue())&&(this.getSuit()==other.getSuit())){
            re=true;
        }
        return re;
}    
    
}
