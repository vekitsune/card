
package hu.veronik.card;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import hu.veronik.card.SuitComparator;


public class Card {
    public static void main(String[]args){
        List<PlayingCard>cards=new ArrayList<>();
        
        PlayingCard first=new PlayingCard();
        first.setSuit(Suit.PIKK);
        first.setValue(Value.V6);
        PlayingCard second=new PlayingCard();
        second.setSuit(Suit.KARO);
        second.setValue(Value.VDAMA);
        PlayingCard third=new PlayingCard();
        third.setValue(Value.V9);
        third.setSuit(Suit.PIKK);
        
        cards.add(third);
        cards.add(second);
        cards.add(first);
        
        
        System.out.println("Rendezetlen lista");
        for (PlayingCard card : cards) {
            System.out.println(card);
            
        }
        Collections.sort(cards, new SuitComparator());
        System.out.println("Rendezett lista");
        for (PlayingCard card : cards) {
            System.out.println(card);
        }
    }
}
