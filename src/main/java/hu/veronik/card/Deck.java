/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.veronik.card;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author User
 */
public interface Deck {
    List<PlayingCard>deck=new ArrayList<>();
    
    public void init();//osszeallitja a paklit
    public void shuffle();//megkever
    public PlayingCard randomCard();
    public PlayingCard draw();//nex Card
    
    
    
}
