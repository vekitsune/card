/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.veronik.card;

import java.util.Comparator;
import hu.veronik.card.PlayingCard;

/**
 *
 * @author User
 */
public class SuitComparator implements Comparator<PlayingCard>{



    @Override
    public int compare(PlayingCard o1, PlayingCard o2) {
       
            return o1.getSuit().getValue()-o2.getSuit().getValue();
       
    }

   
    
}
