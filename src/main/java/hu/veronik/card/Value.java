/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.veronik.card;

/**
 *
 * @author User
 */
public enum Value {
    VAS(1),
    V2(2),
    V3(3),
    V4(4),
    V5(5),
    V6(6),
    V7(7),
    V8(8),
    V9(9),
    V10(10),
    VJUMBO(10),
    VDAMA(10),
    VKING(10),
    VASL(11);
    

    public final int value;
    private boolean asLow=true;
    
    

    Value(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }

}
