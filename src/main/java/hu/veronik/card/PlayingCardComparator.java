/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.veronik.card;

import java.util.Comparator;

/**
 *
 * @author User
 */
public class PlayingCardComparator implements Comparator<PlayingCard>{
    
     SuitComparator suitC=new SuitComparator();
      ValueComaparator valueC=new ValueComaparator();

    @Override
    public int compare(PlayingCard o1, PlayingCard o2) {
      
       if(valueC.compare(o1, o2)==0){
           return suitC.compare(o1, o2);
   }else return valueC.compare(o1, o2);
    }
    
}
