/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.veronik.card;

/**
 *
 * @author User
 */
public enum Suit {
    KARO(0),
    TREFF(1),
    KOR(2),
    PIKK(3);
    
    private final int value;
    
Suit(int value){
    this.value=value;
}
public  int getValue(){
    return this.value;
}
}
