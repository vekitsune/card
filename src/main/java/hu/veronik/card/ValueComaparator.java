/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.veronik.card;

import java.util.Comparator;

/**
 *
 * @author User
 */
public class ValueComaparator implements Comparator<PlayingCard>{
 private boolean smallAs=true;
 
 
    @Override
    public int compare(PlayingCard o1, PlayingCard o2) {
       if(smallAs){
           return o1.getValue().getValue()-o2.getValue().getValue();
       }else{
           if(o1.getValue()==Value.VAS) o1.setValue(Value.VASL);
          if(o2.getValue()==Value.VAS)o2.setValue(Value.VASL);
          return o1.getValue().getValue()-o2.getValue().getValue();
       }
    }

    public boolean isSmallAs() {
        return smallAs;
    }

    public void setSmallAs(boolean smallAs) {
        this.smallAs = smallAs;
    }
    
}
